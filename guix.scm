(define-module (soccoop-pipeline)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix git-download))

(define-public soccoop-pipeline
  (package
    (name "soccoop_pipeline")
    (version "0.1")
    (source
      (origin
        (method git-fetch)
        (uri
          (git-reference
            (url "https://gitlab.com/ddenniss/soccoop-pipeline.git")
            (commit "v0.1")))
        (sha256
          (base32 "1k5xjdqcsmly7xsq5ad7c3227y3csl14xs9ijf19kg303zqxzq3x"))
        ))
    (build-system gnu-build-system)
    (synopsis "Data pipelines to automate Soccoop work")
    (description "Reading, processing and writing data on retailers prices, goods and manufacturers")
    (inputs
      (list openjdk clojure babashka))
    (home-page "https://gitlab.com/ddenniss/soccoop_pipeline")
    (license "GPL3")))

soccoop-pipeline
